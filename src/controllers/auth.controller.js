const ApiError = require('../models/apierror.model')
const pool = require('../config/db')
const config = require('../config/config')
const jwt = require('jsonwebtoken');
var crypto = require('crypto');

module.exports = {

	register(req, res, next) {
		console.log('AuthController.register called')
		var passwordHash = crypto.createHash('md5').update(req.body.password).digest('hex');

		const query = 'INSERT INTO `users` (`name`, `surname`, `email`, `password`, `residence`) VALUES (?, ? ,?, ?, ?)'
		pool.query(
			query,
			[req.body.name, req.body.surname, req.body.email, passwordHash, req.body.residence],
			function (err, rows, fields) {
				if (err) {
					console.log(err.sqlMessage)
					return next(new ApiError(err.sqlMessage, 500))
				}
				if (rows && rows.affectedRows === 1 && rows.warningStatus === 0) {
					// Create a token and return it.
					jwt.sign({
						id: rows.insertId
					}, config.secretkey, (err, token) => {
						if (err) {
							console.log(err)
							return next(new ApiError('Error creating token!', 500))
						}
						// console.log(token);
						res.status(200).json({
							result: {
								name: req.body.name,
								surname: req.body.surname,
								token: token
							}
						}).end()
					});
				} else {
					return next(new ApiError(rows, 500))
				}
			})
	},

	login(req, res, next) {
		console.log('AuthController.login called')

		const email = req.body.email || ''
		const password = req.body.password || ''
		var passwordHash = crypto.createHash('md5').update(password).digest('hex');
		pool.query("SELECT * FROM users WHERE `email` = ? AND `password` = ?", [req.body.email, passwordHash], (err, rows, fields) => {
			if (err) {
				console.log(err)
				return next(new ApiError(err, 500))
			}

			if (rows.length === 1) {
				jwt.sign({
					id: rows[0].ID
				}, config.secretkey, (err, token) => {
					if (err) {
						console.log(err)
						return next(new ApiError('Error creating token!', 500))
					}
					res.status(200).json({
						result: {
							id: rows[0].id,
							name: rows[0].name,
							surname: rows[0].surname,
							token: token
						}
					}).end()
				});
			} else {
				return next(new ApiError('User not found', 500))
			}
		})
	},

	validateJWT(req, res, next) {

		const token = req.header('x-access-token')
		if (!token) {
			return next(new ApiError('Required token is missing', 401))
		}

		jwt.verify(token, config.secretkey, (err, payload) => {
			if (err) {
				return next(new ApiError('Invalid token supplied.', 401))
			}
			console.log('Token successfully validated!')
			console.dir(payload)
			req.user = {
				id: payload.id
			}
			next()
		})
	}

}