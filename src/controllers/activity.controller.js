const ApiError = require('../models/apierror.model')
const pool = require('../config/db')

module.exports = {

	getActivities(req, res, next) {
		pool.query("SELECT * FROM activities", function (err, rows, fields) {
			if (err) {
				console.log(err)
				return next(new ApiError(err, 500))
			}
			res.status(200).json({
				result: rows
			}).end()
		})
	},

	getActivityById(req, res, next) {
		const id = parseInt(req.params.activityId)
		pool.query("SELECT * FROM activities WHERE id = ?", [id], function (err, rows, fields) {
			if (err) {
				console.log(err)
				return next(new ApiError(err, 500))
			} else {
				if (rows.length === 0) {
					return next(new ApiError("User Not found", 500))
				} else {
					res.status(200).json({
						result: rows
					}).end()

				}
			}
		})
	},

	getParticipants(req, res, next) {
		pool.query("SELECT users.id, users.name, users.surname, users.email, users.residence FROM `users` INNER JOIN fk_users_activities ON users.id=fk_users_activities.user_id INNER JOIN activities ON activities.id=?", [parseInt(req.params.activityId)], function (err, rows, fields) {
			if (err) {
				console.log(err)
				return next(new ApiError(err, 500))
			}
			res.status(200).json({
				result: rows
			}).end()
		})
	},

	getParticipantById(req, res, next) {
		pool.query("SELECT users.id, users.name, users.surname, users.email, users.residence FROM `users` INNER JOIN fk_users_activities ON users.id=fk_users_activities.user_id INNER JOIN activities ON activities.id=? Where users.id = ?", [parseInt(req.params.activityId), parseInt(req.params.participantIid)], function (err, rows, fields) {
			if (err) {
				console.log(err)
				return next(new ApiError(err, 500))
			}
			res.status(200).json({
				result: rows
			}).end()
		})
	},

	addActivity(req, res, next) {
		var starttime = new Date(req.body.start_date).toISOString().slice(0, 19).replace('T', ' ')
		var endtime = new Date(req.body.end_date).toISOString().slice(0, 19).replace('T', ' ')
		const query = "INSERT INTO `activities` (`name`, `discription`, `start_date`, `end_date`, `max_participants`, `admin_user_id`) VALUES (?, ?, ?, ?, ?, ?)"
		pool.query(
			query,
			[req.body.name, req.body.discription, starttime, endtime, req.body.max_participants, req.body.admin_user_id],
			function (err, rows, fields) {
				if (err) {
					console.log(err.sqlMessage)
					return next(new ApiError(err.sqlMessage, 500))
				} else {
					res.status(200).json({
						id: rows.insertId
					}).end()
				}
			})

	},
	addParticipant(req, res, next) {
		const id = parseInt(req.params.activityId)
		pool.query("SELECT * FROM fk_users_activities WHERE user_id = ? and activity_id = ?", [req.body.user_id, req.params.activityId], function (err, rows, fields) {
			if (err) {
				console.log(err)
				return next(new ApiError(err, 500))
			} else {
				if (rows.length === 0) {
					const query = "INSERT INTO `fk_users_activities` (`user_id`, `activity_id`) VALUES (?, ?)"
					pool.query(
						query,
						[req.body.user_id, req.params.activityId],
						function (err, rows, fields) {
							if (err) {
								console.log(err.sqlMessage)
								return next(new ApiError(err.sqlMessage, 500))
							} else {
								res.status(200).json({
									id: rows.insertId
								}).end()
							}
						})
					
				} else {
					return next(new ApiError("Already added", 500))

				}
			}
		})

	},
	updateActivity(req, res, next) {
		var starttime = new Date(req.body.start_date).toISOString().slice(0, 19).replace('T', ' ')
		var endtime = new Date(req.body.end_date).toISOString().slice(0, 19).replace('T', ' ')
		const query = "UPDATE `activities` SET `name` = ?, `discription` = ?, `start_date` = ?, `end_date` = ?, `max_participants` = ?, `admin_user_id` = ? WHERE `activities`.`id` = ?"
		pool.query(
			query,
			[req.body.name, req.body.discription, starttime, endtime, req.body.max_participants, req.body.admin_user_id, req.params.activityId],
			function (err, rows, fields) {
				if (err) {
					console.log(err.sqlMessage)
					return next(new ApiError(err.sqlMessage, 500))
				} else {
					res.status(200).json({
						id: rows.insertId
					}).end()
				}
			})
	},
	deleteActivity(req, res, next) {

		const query = "DELETE FROM `activities` WHERE `activities`.`id` = ?"
		pool.query(
			query,
			[req.params.activityId],
			function (err, rows, fields) {
				if (err) {
					console.log(err.sqlMessage)
					return next(new ApiError(err.sqlMessage, 500))
				} else {
					if (rows.affectedRows == 0) {
						return next(new ApiError("Activity Not Found", 500))
					}
					res.status(200).json({message: "deleted"}).end()
				}
			})
	},
	deleteParticipantFromActivity(req, res, next) {
		const query = "DELETE FROM `fk_users_activities` WHERE `activity_id` = ? and `user_id` = ?"
		pool.query(
			query,
			[req.params.activityId, req.params.participantIid],
			function (err, rows, fields) {
				if (err) {
					console.log(err.sqlMessage)
					return next(new ApiError(err.sqlMessage, 500))
				} else {
					res.status(200).json({message: "deleted"}).end()
				}
			})
	}


}