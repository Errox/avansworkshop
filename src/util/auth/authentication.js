const settings = require('../../config/config')
const moment = require('moment');
const jwt = require('jsonwebtoken');


module.exports = {

    encodeToken(username) {
        const playload = {
            exp: moment().add(10, 'days').unix(),
            iat: moment().unix(),
            sub: username
        }
        return jwt.sign(payload, settings.secretkey);
    },

    decodeToken(token, callback) {

        try {
            const payload = jwt.sign(token, settings.secretkey)

            const now = moment().unix();

            if (now > payload.exp) {
                callback('token is rotten', null)
            } else {
                callback(null, payload)
            }

        } catch (err) {
            callback(err, null)
        }
    }
}