const router = require('express').Router()
const activityController = require('../controllers/activity.controller')
const authController = require('../controllers/auth.controller')


//[Get]
//Get all activities
router.get('/activities', activityController.getActivities)
//Return activity based on ID
router.get('/activities/:activityId', activityController.getActivityById)
//Return all participants of activity
router.get('/activities/:activityId/participants', activityController.getParticipants)
//Return specific participants of activity
router.get('/activities/:activityId/participants/:participantIid', activityController.getParticipantById)

//[Post]
//Add new activity (Request body: new activity)
router.post('/activities', activityController.addActivity)
//Add participant to activity
router.post('/activities/:activityId/participants', activityController.addParticipant)

//[Put]
//Update activity based on id. (Request body: edited activity) 
router.put('/activities/:activityId', activityController.updateActivity)

//[Delete]
//Delete activity with given id
router.delete('/activities/:activityId', activityController.deleteActivity);
//Delete participant form activity.
router.delete('/activities/:activityId/participants/:participantIid', activityController.deleteParticipantFromActivity)

router.post('/register', authController.register)
router.post('/login', authController.login)


module.exports = router