const chai = require('chai')
const chaiHttp = require('chai-http')
const server = require('../server')
const pool = require('../src/config/db')
const sinon = require('sinon');
const assert = require('assert');
var crypto = require('crypto');

const sandbox = sinon.createSandbox();
const jwt = require('jsonwebtoken');
chai.should()
chai.use(chaiHttp)
var date = new Date();
const endpointToTest = '/api/activities'

const activitie = {
    name: 'name',
    discription: 'discription',
    max_participants: 5,
    start_date: new Date(),
    end_date: new Date()
}
const user = {
    name: 'name',
    surname: 'surname',
    email: 'user@server.com',
    password: 'secret',
    residence: 'place'
}

describe('Activities API GET', () => {
    before((done) => {
        sandbox.stub(jwt, 'verify').callsArgWith(2, null, {});
        const query = 'DELETE FROM `users`'
        pool.query(query, (err, rows, fields) => {
            if (err) {
                assert.fail(err)
            } else {
                const query = 'INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password` , `residence`) VALUES (1,?, ?, ?, ?, ? )'
                var passwordHash = crypto.createHash('md5').update(user.password).digest('hex');
                const values = [user.name, user.surname, user.email, passwordHash, user.residence]
                pool.query(query, values, (err, rows, fields) => {
                    if (err) {
                        assert.fail(err)
                    } else {

                        const query = 'DELETE FROM `activities`'
                        pool.query(query, (err, rows, fields) => {
                            if (err) {
                                assert.fail(err)
                            } else {
                                const query = 'INSERT INTO `activities` (`id`,`name`, `discription`,  `start_date` , `end_date`, `max_participants` ,`admin_user_id`) VALUES (1, ?, ?, ?, ?,?,1)'
                                const values = [activitie.name, activitie.discription, activitie.start_date, activitie.end_date, activitie.max_participants]
                                pool.query(query, values, (err, rows, fields) => {
                                    if (err) {
                                        assert.fail(err)
                                    } else {
                                        pool.query('DELETE FROM `fk_users_activities`', (err, rows, fields) => {
                                            if (err) {
                                                assert.fail(err)
                                            } else {
                                                pool.query('INSERT INTO `fk_users_activities` (`user_id`, `activity_id`) VALUES (1, 1)', (err, rows, fields) => {
                                                    if (err) {
                                                        assert.fail(err)
                                                    } else {
                                                        done();
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })

                    }
                })
            }
        })
    });

    it('should return an array of Activities', (done) => {
        chai.request(server)
            .get(endpointToTest)
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.body.result.should.be.an('array')
                res.body.result.should.have.length(1)
                done()
            })
    })
    it('should return an  Activity based with the correct id', (done) => {
        chai.request(server)
            .get(endpointToTest + "/1")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.body.result.should.be.an('array')
                res.body.result.should.have.length(1)
                res.body.result[0].should.have.property('name')
                res.body.result[0].should.have.property('discription')
                res.body.result[0].should.have.property('start_date')
                res.body.result[0].should.have.property('end_date')
                res.body.result[0].should.have.property('max_participants')
                res.body.result[0].should.have.property('admin_user_id')
                done()
            })
    })
    it('should return an error based with the wrong id', (done) => {
        chai.request(server)
            .get(endpointToTest + "/999")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(500)
                done()
            })
    })

    it('should return participents of given activity', (done) => {
        chai.request(server)
            .get(endpointToTest + "/1/participants")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.body.result.should.be.an('array')
                res.body.result.should.have.length(1)
                res.body.result[0].should.not.have.property('password')
                res.body.result[0].should.have.property('name')
                res.body.result[0].should.have.property('surname')
                res.body.result[0].should.have.property('email')
                res.body.result[0].should.have.property('residence')
                done()
            })
    })

    it('should return nothing for getting participants of wrong id', (done) => {
        chai.request(server)
            .get(endpointToTest + "/999/participants")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.result.should.have.length(0)
                done()
            })
    })

    it('should return a specific user of given activity', (done) => {
        chai.request(server)
            .get(endpointToTest + "/1/participants/1")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.be.a('object')
                res.body.result.should.be.an('array')
                res.body.result.should.have.length(1)
                res.body.result[0].should.not.have.property('password')
                res.body.result[0].should.have.property('name')
                res.body.result[0].should.have.property('surname')
                res.body.result[0].should.have.property('email')
                res.body.result[0].should.have.property('residence')
                done()
            })
    })

    it('should return a valid activity when posting a valid activity', (done) => {

        chai.request(server)
            .post(endpointToTest)
            .set('x-access-token', 'anything')
            .send({
                'name': '  somename  ',
                'discription': '  discription   ',
                'start_date': new Date().toISOString().slice(0, 19).replace('T', ' '),
                'end_date': new Date().toISOString().slice(0, 19).replace('T', ' '),
                'max_participants': 1,
                'admin_user_id': 1
            })
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.have.property('id')
                done()
            })
    })

    it('should return error when posting a invalid activity', (done) => {

        chai.request(server)
            .post(endpointToTest)
            .set('x-access-token', 'anything')
            .send({
                'name': '  somename  ',
                'discription': '  discription   ',
                'start_date': "today",
                'end_date': "today",
            })
            .end((err, res) => {
                res.should.have.status(500)
                done()
            })
    })

    it('should return right status when add user to activity', (done) => {

        chai.request(server)
            .post(endpointToTest + "/1/participants")
            .set('x-access-token', 'anything')
            .send({
                'user_id': 2
            })
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.have.property('id')
                done()
            })
    })
    it('should return right status when updating activity', (done) => {
        chai.request(server)
            .put(endpointToTest + "/1")
            .set('x-access-token', 'anything')
            .send({
                'name': '  somename  ',
                'discription': '  discription   ',
                'start_date': new Date().toISOString().slice(0, 19).replace('T', ' '),
                'end_date': new Date().toISOString().slice(0, 19).replace('T', ' '),
                'max_participants': 1,
                'admin_user_id': 1
            })
            .end((err, res) => {
                res.should.have.status(200)
                res.body.should.have.property('id')
                done()
            })
    })
    it('delete activity without error', (done) => {
        chai.request(server)
            .del(endpointToTest + "/1")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })
    it('delete not existing activity with error', (done) => {
        chai.request(server)
            .del(endpointToTest + "/999")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(500)
                done()
            })
    })
    it('delete activity participants without error', (done) => {
        chai.request(server)
            .del(endpointToTest + "/1/participants/1")
            .set('x-access-token', 'anything')
            .end((err, res) => {
                res.should.have.status(200)
                done()
            })
    })
    after(() => {
        sandbox.restore();
    })
})