const chai = require('chai')
const chaiHttp = require('chai-http')
const assert = require('assert')
const server = require('../server')
const pool = require('../src/config/db')
var crypto = require('crypto');

chai.should()
chai.use(chaiHttp)
var token;
const user = {
    name: 'name',
    surname: 'surname',
    email: 'user@server.com',
    password: 'secret',
    residence: 'place'
}

describe('Auth API', function () {


    before((done) => {
        const query = 'DELETE FROM `users`'
        pool.query(query, (err, rows, fields) => {
            if (err) {
                assert.fail(err)
            } else {
                const query = 'INSERT INTO `users` (`id`, `name`, `surname`, `email`, `password` , `residence`) VALUES (1,?, ?, ?, ?, ? )'
                var passwordHash = crypto.createHash('md5').update(user.password).digest('hex');
                const values = [user.name, user.surname, user.email, passwordHash, user.residence]
                pool.query(query, values, (err, rows, fields) => {
                    if (err) {
                        assert.fail(err)
                    } else {
                        done();
                    }
                })
            }
        })
    });

    it('creates a user on valid registration', (done) => {

        var user = {
            email: 'user2@server.com',
            password: 'secret2',
            name: 'name2',
            surname: 'surname2',
            residence: 'residence2'
        }
        chai.request(server)
            .post('/api/register')
            .send(user)
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');
                res.body.result.should.have.property('name').that.is.a('string');
                res.body.result.should.have.property('surname').that.is.a('string');
                res.body.result.should.have.property('token').that.is.a('string');
                done();
            });
    })

    it('returns an error on invalid registration', (done) => {
        var user = {}
        chai.request(server)
            .post('/api/register')
            .send(user)
            .end(function (err, res) {
                res.should.have.status(500);
                done();
            });
    })

    it('returns an error on invalid login', (done) => {

        var user = {}
        chai.request(server)
            .post('/api/login')
            .send(user)
            .end(function (err, res) {
                res.should.have.status(500);
                done();
            });
    })
    it('returns a token on valid login', function (done) {
        var user = {
            email: 'user@server.com',
            password: 'secret',
        }
        chai.request(server)
            .post('/api/login')
            .send(user)
            .end(function (err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.an('object');
                res.body.result.should.have.property('token').that.is.a('string');
                done();
            });
    });
});