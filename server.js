const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const activityRoutes = require('./src/routes/activity.routes')
const auth_routes = require('./src/routes/auth.routes')
const ApiError = require('./src/models/apierror.model')
const authController = require('./src/controllers/auth.controller')
	
var app = express();

app.use(morgan('dev'))
app.use(bodyParser.json())

const port = process.env.PORT || 3000

// auth routing
app.use('/api', auth_routes)

//Authenticate
app.all('*', authController.validateJWT);

//ApiRoutes
app.use('/api', activityRoutes)


// Handling non existing routes
app.use('*', (req, res, next) => {
	next(new ApiError('Non-existing endpoint', 404))
})

// handler voor errors
app.use('*', (err, req, res, next) => {
	console.dir(err)
	res.status(err.code).json({error: err}).end()
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

// for testing purpose
module.exports = app
